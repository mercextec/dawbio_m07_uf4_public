<!--Navbar-->
<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <!-- Navbar brand -->
        <a class="navbar-brand" href="{{ url('/home') }}">
            <img src="{{ asset('img/proven.jpg') }}" alt="logo" width="75" height="50">
            {{ config('app.name') }}                 
        </a>
        <!--  Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
         <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/home') }}">Home
                    </a>
                </li>
                <!-- Dropdown -->
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Categories</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ url('/category/list') }}">List all</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="{{ route('catfind') }}">Find & Edit</a></li>
                        <li><a class="dropdown-item" href="{{ route('catcreate') }}">Create</a></li>
                    </ul>
                </li> 
            </ul>

          
        </div>
</nav>
