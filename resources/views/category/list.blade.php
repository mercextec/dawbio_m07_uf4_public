@extends('home')

@section('content')
    @isset($arrCategories)
    <div class="card">
        <div class="card-header text-center">
           List categories
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <tr class="thead-dark">
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                @foreach ($arrCategories as $category) 
                    <tr>
                        <td><a href="{{ url('/category/edit/' . $category->id) }}"/>{{ $category->id }}</a></td>
                        <td><a href="{{ url('/category/edit/' . $category->id) }}"/>{{ $category->name }}</a></td>
                        <td>{{ $category->description }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endisset
@stop

@section('message')
    @isset($message)
        <div class="alert alert-warning">
            {{ $message }}
        </div>
    @endisset
@stop