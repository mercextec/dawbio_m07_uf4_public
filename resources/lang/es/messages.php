<?php

// Spanish
return [

    'prod' => 'producto',
    'prods' => 'Productos',
    
    'cat' => 'categoria',
    'cats' => 'Categorías',    
    
    'create_ok' => 'Datos insertados correctamente',

];
