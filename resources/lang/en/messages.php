<?php

// English
return [

    'prod' => 'product',
    'prods' => 'Products',
    
    'cat' => 'category',
    'cats' => 'Categories',    
    
    'create_ok' => 'Data inserted successfully',

];

