<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        //BEGIN - manual modifications
        $this->call(CategoriesTableSeeder::class);
        //END - manual modifications
    }
}
