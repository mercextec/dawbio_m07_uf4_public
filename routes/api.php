<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('categories', 'API\CategoryController');

//amb apiResource queden definides les routes:
// -------------------------------------------------------------------
// | Verb    | URI                    | Method  | Route Name         |
// -------------------------------------------------------------------
// | GET     | /categories            | index   | categories.index   |
// | POST    | /categories            | store   | categories.store   |
// | GET     | /categories/{category} | show    | categories.show    |
// |PUT/PATCH| /categories/{category} | update  | categories.update  |
// | DELETE  | /categories/{category} | destroy | categories.destroy |
// -------------------------------------------------------------------


