<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//BEGIN - exemples routes

Route::get('/hello', function ()  
 {      
return "Hello World";  
});
//redirect exemple
Route::redirect('hello2','hello'); 

//parameter
Route::get('/hello/{name}', function($name)  
{  
  return "Hello ".$name;   
}
);

//several parameters
Route::get('/params/{id}/{name}', function($id,$name)  
{  
  return "id number is : ". $id ." ".$name;   
}
);

//optional parameter
Route::get('hello3/{name?}', function ($name = 'Maria') {  
    return "Hola ".$name;  
}); 

//END - exemples routes

//BEGIN - categories

Route::view('/home', 'home');
//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/category/list', 'CategoryController@listAll');


Route::view('/category/find', 'category.find')->name('catfind');
Route::post('/category/find', 'CategoryController@find');

Route::get('/category/edit/{id}', 'CategoryController@edit')->name('catedit');
Route::post('/category/modify', 'CategoryController@modify');  //update&delete

Route::view('/category/create', 'category.create')->name('catcreate');
Route::post('/category/create', 'CategoryController@create');
 
//END - categories