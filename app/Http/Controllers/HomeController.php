<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //BEGIN - manual modifications
        public function index()
    {
        return view('home');
    }
    //END - manual modifications
}
