<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//BEGIN - manual modification 
use App\Category;

//END - manual modification

class CategoryController extends Controller {

    //BEGIN - manual modifications
    public function listAll() {
        try {
            $arrCat = Category::all();

            if (isset($arrCat)) {
                $message = count($arrCat) . " categories found";
            } else {
                $message = "No data found";
            }
        } catch (\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }

        return view('category.list', ['arrCategories' => $arrCat, 'message' => $message]);
    }

    // find first version - show results using 'category.find' view
    /*
     *     public function find(Request $request) { 
      try {
      $objCat = Category::findOrFail($request->id);

      //return redirect()->to('/category/edit/' . $request->id);
      } catch(\Exception $e) {
      $message = "No data found - " . $e->getMessage();
      return view('category.find', ['message' => $message]);
      }

      //return view('category.find', ['id' => $request->id, 'message' => $message]);
      return view('category.find', ['id' => $objCat->id, 'name' => $objCat->name]);
      }
     */

    // find second version - when found redirect to categoy/edit route
    public function find(Request $request) {
        try {
            $objCat = Category::findOrFail($request->id);  // exception when not found
            return redirect()->to('/category/edit/' . $request->id);
        } catch (\Exception $e) {
            $message = "No data found - " . $e->getMessage();
            return view('category.find', ['message' => $message]);
        }
        return view('category.find', ['id' => $request->id, 'message' => $message]);
    }

    public function edit($id) {
        try {
            $objCat = Category::findOrFail($id);

            $message = "Data found";
            //$message=Session::get('message'); //TODO (amb auth) 

            return view('category.edit', ['objCategory' => $objCat, 'message' => $message]);
        } catch (\Exception $e) {
            $message = "No data found - " . $e->getMessage();
        }

        //return view('category.find', ['id' => $id, 'message' => $message]);
        return redirect()->to('fallback');
    }

    //mateix metod per update & delete, canvia el action del form
    public function modify(Request $request) {
        switch ($request->action) {
            case "update":
                return $this->update($request);
            case "delete":
                return $this->delete($request);
        }
    }

    //validations when updating a category
    public function check_update(Request $request) {
        $alphabetic = "/^[a-z ]*$/i";

        $request->validate([
            'name' => "required|regex:$alphabetic|min:1|max:50",
            'description' => "nullable|string|max:100",
        ]);
    }

    //update a category        
    public function update($request) {
        $this->check_update($request);   //validacions pel update      

        try {
            $objCat = Category::findOrFail($request->id);

            $objCat->update($request->all());

            $message = "Data updated successfully";

            return redirect()->back()->with('message', $message);
        } catch (\Exception $e) {
            $message = "No data updated - " . $e->getMessage();
        }

        return view('category.edit', ['objCategory' => $objCat, 'message' => $message]);
    }

    //validations when deleting a category
    public function check_delete(Request $request) {

        //TODO check - no product belongs to the category to be deleted
    }

    //delete a category
    public function delete($request) {
        $this->check_delete($request);  //validacions pel delete

        try {
            $objCat = Category::findOrFail($request->id);

            $objCat->delete();
            //$affectedRows = $objCat->delete();

            $message = "Data deleted successfully";

            return view('category.find', ['message' => $message]);
        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == 23000) {
                $message = "No data deleted - Category is used by product - " . $e->getCode();
            }
        } catch (\Exception $e) {
            $message = "No data deleted - " . $e->getMessage();
        }

        return view('category.edit', ['objCategory' => $objCat, 'message' => $message]);
    }

    //validacions abans del create
    public function check_create(Request $request) {

        // sólo letras y espacio
        $alphabetic = "/^[a-z ]*$/i";

        $request->validate([
            'name' => "required|unique:categories|regex:$alphabetic|min:1|max:50",
            'description' => "nullable|string|max:100",
        ]);


        /*
          // https://laravel.com/docs/7.x/validation

          // obtener los campos validados
          $validator = Validator::make($request->all(), [
          'name' => "required|regex:$alphabetic|min:1|max:50",
          'description' => "nullable|string|max:100",
          ]);

          // obtener los errores de validación
          if ($validator->fails()) {
          return $validator->errors();
          }

          $numeric="/^[0-9]*$/";
          $phone="/^[0-9]{9}$/";
          $postal_code="/^[0-9]{5}$/";
         */
    }

    //afegir una categoria
    public function create(Request $request) {
        $this->check_create($request);

        try {
            // per crear amb tots els camps
            Category::create($request->all());

            /*
              // si volem fer create amb alguns camps
              $cat = new Category;
              $cat->name = $request->name;
              $cat->description = $request->description;
              $cat->save();

              // obtenir l'identificador despres del create (el genera la BD)
              $idNew = $cat->id;
             */

            //$message = 'Datos insertados correctamente';
            $message = trans('messages.create_ok'); //TODO - multilanguage

            $request = NULL;
        } catch (\Exception $e) {
            $message = "No data inserted - " . $e->getMessage();
        }

        return view('category.create', ['objCategory' => $request, 'message' => $message]);
    }

    //END - manual modifications
}
